# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

try:
    long_description = open("README.rst").read()
except IOError:
    long_description = ""

setup(
    name="granatum_sdk",
    version="0.1.0",
    description="SDK for g-boxes",
    license="MIT",
    author="Xun",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'pandas==0.24.2',
        'numpy==1.16.3',
        'scanpy==1.4.1',
    ],
    long_description=long_description,
    classifiers=[]
)
